# Convolutional Neural Networks

Convolutional Neural Networks are the networks which are good at finding patterns and then using them to classify images according to the need.
>Convolutional Neural Networks recieve images as input and with some computation over the layers, these images gets classified or predicted.

## What is Convolution?
Convolution or convolving is the process where some step is repeated over and over again over a particular thing.


## What goes on inside a CNN?
A CNN or Convolutional Neural Network primarily consists of different combinations of multiple layers :
* Convolutional Layer
* Activation Layer
* Pooling Layer 
* Fully Connected Layer
When these layers are combined in different orders of requirement produce a Neural Network which is capable of detecting patterns,shapes,objects etc and thus finally predicting the image.
![CNN structure](../images/CNN.png)

## Convolutional Layer
This is the first layer after the input layer and is used to detect different patterns, shapes, edges etc.
Convolutional layer recieves a matrix which is formed by the brightness of the pixels of the image.Now,the main working of this layers depend upon the **Filters**.Filters are basically a very small matrix usually consisting of odd number of elements so that is consists of a central element.These Filters can be of various types such as- __Edge Detecting Filters__,__Shape Detecting Filters__,etc.These filters are the main parts of this layer as they help to find different patterns,edges etc.
So these filters have some random value and consists of very small no of rows or columns, now the main image matrix is subdivided into many parts where each part consists of the same number of element as of the filter.Now the elements in the filter are multiplied with each of the element of the sub-division and added to form an element of a new matrix which is formed by multiplying each of the sub-divisions with the filter matrix  and then adding them up.
>This operation of repeating the multiplication and addition of matrices is knows as Convolution or Convolving.
![Convolution](../images/Convolve.png)


This process of convolution consists of some parameters -
* **Grid or Filter Size**
Filter size is the size of the filter which is to be decided yourself in the initial cycle of the CNN.
* **Padding**
Basically, when the filter is passed over the sub-divisions it was seen that the outermost elements were given very less importance hence, we introduced Padding.Padding is nothing but a row and column of 0's surrounding the matrix.
![Paddin](../images/padding.png)

*  **Stride**
Stride is defined as the size of the steps of the filter layer i.e every multiplication operatiion is done after how many rows or columns.
![Stride](../images/Stride.png)


## Activation Layer
In this layer, all the negative values produced after multiplication are converted into 0's as they have no weightage in the classification.
This also provides a better and cleaner structure to the matrix.
Most widely used activation function in CNNs is  **ReLU**.

## Pooling Layer
In the Pooling Layer, a process known as Max-Pooling is implemented.In Max Pooling, the most weighted elements or activation is considered in a selected range of rows and columns and these selected elements will now represent all their respective ranges.Through max-pooling the learning parameters are reduced and the model becomes more easy to train also it reduces the variance of the elements inside the matrix and also resulting in a smaller matrix.
![MaxPool](../images/MaxPool.png)


## Fully Connected Layer
A fully connected layer is just like a simple neural network where the model trains itself to recognize the images by forward propogation,generating cost an back propogating itself so as to predict the image with more accuracy
![Full CNN](../images/Cnn.jpeg)
