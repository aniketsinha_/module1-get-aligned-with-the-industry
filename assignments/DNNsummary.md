# Neural Networks
## Simple Neural Networks
A neural network is a computational model inspired by Animal or Human Brains. A neural network is used to generate results or predictions for some data or image usually like a human brain.
Usually Neural Networks consists of nodes in form of layers - an __Input Layer__, a __Hidden Layer__ and in the last- the __Ouptut Layer__.

<img src='../images/simple_neural.png' height="500"/>

# Deep Neural Networks(DNN)
A DNN is similar to a Simple Neural Network but with one key difference i.e -
__A DNN consists of *more than One Hidden Layers* in between the Input Layer and the Output Layer whereas a Simple Neural Network consists of only One Hidden Layer.__


![deep-neural-network](../images/deep-neural-network.jpg)

## What happens inside a Neural Network?
The basic functioning of a Neural Network is as follows:
* A Training Set is provided to the Input Layer of the network the Output Layer is expected to guess or predict the particular item or image on the basis of the labels present according to the dataset.
* The values inside the nodes give us information about the activation of the particular Node and finally the node which has the highest value in the Output Layer is considered as the prediction of the Network.
* The Strings work as Weights which basically connects two layers and define the importance or weightage of the node in the previous Layer.
* The Hidden Layers are used for processing the Data or Image according to different criterions.

![weights and activation inside DNN](../images/Inside-DNN.png)

## Flow of a Deep Neural Network
A dataset is passed through the input layer in terms of vector of the pixels or the values.
The neurons operates the vector to perform a linear transformation using the weights.
**Bias-** A constant is added to this transformation so as to set a lower limit in the selection critera for the prediction.

So the activation function is defined as:
**Y=Activation(Σ(weight * input) + bias)**

This Activation function is applied on every transition from one layer to another.

>As this is the first forward propogation we set the values of the Bias and the Weights as random values.These random values provide us with a random answer or prediction.

<img src='../images/basic-network.png' height=500/>

## Activation Functions
The main functions of an Activation function is to compress the result we got from **Σ(weight * input) + bias** into a specific range which then gives us an idea about whether the particular neuron should be activated or not.

![activation function](../images/activationfunction.png)

There are many activation functions but some common examples are as follows:
 * ### Sigmoid:
 Sigmoid functions compresses the result into a specified range of **0 to 1** which thus helps in the comparision of the activations.Since the probability of anything exists from 0 to 1 hence it was considered as a good choice for activation functions.
 
 <img src='../images/sigmoid.png' height=300/>

 * ### ReLU
 ReLU is a non-Linear Activation Function which is now preferred over sigmoid and other activation functions. ReLU stands for Rectified Linear Unit. The main advantage of ReLU over other activation functions is that it does not activate all neurons at the same time.
![ReLU](../images/relu.png)

## Cost Function
As the model initially from the input layer to the output layer consisted of random values for the activations and the weights, hence the prediction is very vague and confusing.
So now, to inform the network about its mistakes we use a function known as Cost Function. A Cost function takes the values of all the activations for the output nodes and apply itself on these values with the values which were supposed to be accurate. 
>In simpler terms, a Cost Function provides a basic idea of how good or bad the neural network has performed.

>The model is considered Good if the value of the cost function is less and bad if the values is more.

![Cost function](../images/Cost.jpg)

## Gradient Descent 
A Gradient can basically be understood as **the rate of change of a function in a positive manner**. Hence a Gradient will always increase the function.
**Gradient Descent is the negative of Gradient** i.e. it points towards the minimum of the function. Applying this on the Cost Function will provide us a way to decrease the Cost Function hence achieve more accuracy.
![Gradient Descent](../images/Gradient-descent.png)

## Back Propagation
After calculating the weights and the activations on which the model would perform more accurately using the Cost Function and the Gradient Descent, the model activation and weights have to be changed. This is achieved by the process of Back Propagation in we go from output to input layer ie reversed direction and removing the weights and links which  have no or very negligible effect on the model. Hence completing the learning process of the Neural Network.

![Backpropagation](../images/backpropagation.png)