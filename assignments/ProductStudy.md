# Industrial Product Study
## 1. Product Name
RefaceAI
## Product Link
[Link to Product Website](https://reface.ai/)
## Product Short description
RefaceAI uses state of the art technology for swapping faces from your photo to a video or another image.RefaceAI uses the captured photo and detects the face of the person and then the user can select another video or image on which the detected face will be superimposed very accurately. The model also tries to fit the swapped face in such a way that it looks very real and almost seems that the face is rendered in 3D. 
## Product is combination of features
The Product detects the person and then his/her face using **Facial Recognition** models and then uses **Segmentation** to create a model with only the features of the face of the person and then again uses **Facial Recognition** to get the face in the output video or image and then *Superimposes* the face from the first photo on the output video/image.
## Product is provided by which company?
RefaceAI


![RefaceAI](../images/RefaceAI.jpg)

<hr/>

## 2. Product Name
Clarifai
## Product Link
[Link to Product Website](https://www.clarifai.com/)
## Product Short description
Clarifai is a product that has been trained to See,Identify and Categorize images on the Internet. With a large database of 11,000 different images Clarifai takes the image as an input and runs the model over the image which then tags the objects present inside the image with more than 95% accuracy.The platform provides an efficient image processing model to detect products and is even being used in Medical industry to diagnose an injuries and serious medical  conditions.
## Product is combination of features
* Object Detection
* Person Detection
* Image Search using tags and labels
## Product is provided by which company?
Clarifai


![Clarifai](../images/Clarifai.jpg)

<hr/>

## 3. Product Name
Vision AI
## Product Link
[Link to Product Website](https://cloud.google.com/vision)
## Product Short description
Vision AI  by Google is provided with both GUI as well as API for integration purposes. Vision AI detects faces and objects in a image with state of the art algorithms and with a large accuracy.Vision AI is also capable to detect moods, expressions and facial features of a person and also can identify each person or object in an image.Vision AI has 2 sub parts of itself i.e **AutoML Vision** and **Vision API**.The AutoML Vision is a GUI platform where a developer can see understand and use the technology whereas the Vision API provides an API to get integrated with almost any technology.
## Product is combination of features
* Object Detection
* Person Detection
* Emotion Detection
* ID Recognition
## Product is provided by which company?
Google LLC


![Vision AI](../images/VisionAI.svg)
